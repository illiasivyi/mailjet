package services

import (
	"encoding/json"

	"github.com/mailjet/mailjet-apiv3-go"
	"github.com/streadway/amqp"
)

type deliveryContent struct {
	Destination  string `json:"destination"`
	ReceiverName string `json:"receiver_name"`
	Link         string `json:"link"`
}

func (s *service) processDelivery(deliveryRaw *amqp.Delivery) {
	var delivery deliveryContent
	err := json.Unmarshal(deliveryRaw.Body, &delivery)
	if err != nil {
		s.log.Error(err)
		return
	}

	payload := []mailjet.InfoMessagesV31{{
		From: &mailjet.RecipientV31{
			Name:  "Ilya",
			Email: "illiasivyi@gmail.com",
		},
		To: &mailjet.RecipientsV31{
			mailjet.RecipientV31{
				Name:  delivery.ReceiverName,
				Email: delivery.Destination,
			},
		},
		Subject:  "Verification",
		TextPart: "Link to verify your account: " + delivery.Link},
	}

	mail := mailjet.MessagesV31{Info: payload}
	_, err = s.mailjetCli.SendMailV31(&mail)
	if err != nil {
		s.log.WithError(err).Error("failed to sent message")
	}
}
