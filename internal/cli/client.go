package cli

import (
	"github.com/cockroachdb/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/illiasivyi/mailjet/internal/config"
	"gitlab.com/illiasivyi/mailjet/internal/mailjet"
	"gitlab.com/illiasivyi/mailjet/internal/rabbitmq"
	"gitlab.com/illiasivyi/mailjet/services"
)

func Run() {
	log := logrus.New()

	cfg, err := config.NewConfig()
	if err != nil {
		panic(errors.Wrap(err, "failed to setup config"))
	}

	rabbitmqCli := rabbitmq.NewConsumer(*cfg)
	mailjetCli := mailjet.NewClient(*cfg)

	services.Run(*cfg, rabbitmqCli, log, mailjetCli)
}
