package mailjet

import (
	"github.com/mailjet/mailjet-apiv3-go"
	"gitlab.com/illiasivyi/mailjet/internal/config"
)

type Client interface {
	SendMailV31(data *mailjet.MessagesV31) (*mailjet.ResultsV31, error)
}

func NewClient(cfg config.Config) Client {
	return mailjet.NewMailjetClient(cfg.MailjetAPIKey, cfg.MailjetSecretKey)
}
