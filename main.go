package main

import (
	"gitlab.com/illiasivyi/mailjet/internal/cli"
)

func main() {
	cli.Run()
}
